# German translation of the Debian webwml modules
# Copyright (c) 2004 Software in the Public Interest, Inc.
# Dr. Tobias Quathamer <toddy@debian.org>, 2004, 2005, 2006.
# Helge Kreutzmann <debian@helgefjell.de>, 2007, 2008.
# Holger Wansing <linux@wansing-online.de>, 2011, 2014.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml templates\n"
"PO-Revision-Date: 2014-02-14 23:58+0100\n"
"Last-Translator: Holger Wansing <linux@wansing-online.de>\n"
"Language-Team: Debian l10n German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Translators: string printed by gpg --fingerprint in your language, including spaces
#: ../../english/CD/CD-keys.data:4 ../../english/CD/CD-keys.data:8
#: ../../english/CD/CD-keys.data:13
msgid "      Key fingerprint"
msgstr "  Schl.-Fingerabdruck"

#: ../../english/devel/debian-installer/images.data:87
msgid "ISO images"
msgstr "ISO-Abbilder"

#: ../../english/devel/debian-installer/images.data:88
msgid "Jigdo files"
msgstr "Jigdo-Dateien"

#. note: only change the sep(arator) if it's not good for your charset
#: ../../english/template/debian/cdimage.wml:10
msgid "&middot;"
msgstr "&middot;"

#: ../../english/template/debian/cdimage.wml:13
msgid "<void id=\"dc_faq\" />FAQ"
msgstr "FAQ"

#: ../../english/template/debian/cdimage.wml:16
msgid "Download with Jigdo"
msgstr "Mit Jigdo herunterladen"

#: ../../english/template/debian/cdimage.wml:19
msgid "Download via HTTP/FTP"
msgstr "Über HTTP/FTP herunterladen"

#: ../../english/template/debian/cdimage.wml:22
msgid "Buy CDs or DVDs"
msgstr "CDs oder DVDs kaufen"

#: ../../english/template/debian/cdimage.wml:25
msgid "Network Install"
msgstr "Netzwerk-Installation"

#: ../../english/template/debian/cdimage.wml:28
msgid "<void id=\"dc_download\" />Download"
msgstr "Herunterladen"

#: ../../english/template/debian/cdimage.wml:31
msgid "<void id=\"dc_misc\" />Misc"
msgstr "Verschiedenes"

#: ../../english/template/debian/cdimage.wml:34
msgid "<void id=\"dc_artwork\" />Artwork"
msgstr "Druckvorlagen"

#: ../../english/template/debian/cdimage.wml:37
msgid "<void id=\"dc_mirroring\" />Mirroring"
msgstr "Spiegeln"

#: ../../english/template/debian/cdimage.wml:40
msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
msgstr "Rsync-Spiegel"

#: ../../english/template/debian/cdimage.wml:43
msgid "<void id=\"dc_verify\" />Verify"
msgstr "Verifizieren"

#: ../../english/template/debian/cdimage.wml:46
msgid "<void id=\"dc_torrent\" />Download with Torrent"
msgstr "Mit Torrent herunterladen"

#: ../../english/template/debian/cdimage.wml:49
msgid "<void id=\"dc_relinfo\" />Image Release Info"
msgstr "Image-Release-Informationen"

#: ../../english/template/debian/cdimage.wml:52
msgid "Debian CD team"
msgstr "Debian-CD-Team"

#: ../../english/template/debian/cdimage.wml:55
msgid "debian_on_cd"
msgstr "Debian auf CD"

#: ../../english/template/debian/cdimage.wml:58
msgid "<void id=\"faq-bottom\" />faq"
msgstr "FAQ"

#: ../../english/template/debian/cdimage.wml:61
msgid "jigdo"
msgstr "jigdo"

#: ../../english/template/debian/cdimage.wml:64
msgid "http_ftp"
msgstr "HTTP/FTP"

#: ../../english/template/debian/cdimage.wml:67
msgid "buy"
msgstr "Kaufen"

#: ../../english/template/debian/cdimage.wml:70
msgid "net_install"
msgstr "Netz-Installation"

#: ../../english/template/debian/cdimage.wml:73
msgid "<void id=\"misc-bottom\" />misc"
msgstr "Verschiedenes"

#: ../../english/template/debian/cdimage.wml:76
msgid ""
"English-language <a href=\"/MailingLists/disclaimer\">public mailing list</"
"a> for CDs/DVDs:"
msgstr ""
"Englischsprachige <a href=\"/MailingLists/disclaimer\">öffentliche "
"Mailingliste</a> für CDs/DVDs:"
