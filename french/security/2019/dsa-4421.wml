#use wml::debian::translation-check translation="5357ead7bb298e3857977fea7b0087543c6072b3" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le navigateur web
Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5787">CVE-2019-5787</a>

<p>Zhe Jin a découvert un problème d'utilisation de mémoire après
libération.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5788">CVE-2019-5788</a>

<p>Mark Brand a découvert un problème d'utilisation de mémoire après
libération dans l'implémentation de FileAPI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5789">CVE-2019-5789</a>

<p>Mark Brand a découvert un problème d'utilisation de mémoire après
libération dans l'implémentation de WebMIDI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5790">CVE-2019-5790</a>

<p>Dimitri Fourny a découvert un problème de dépassement de tampon dans la
bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5791">CVE-2019-5791</a>

<p>Choongwoo Han a découvert un problème de confusion de type dans la
bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5792">CVE-2019-5792</a>

<p>pdknsk a découvert un problème de dépassement d'entier dans la
bibliothèque pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5793">CVE-2019-5793</a>

<p>Jun Kokatsu a découvert un problème de droits dans l'implémentation des
extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5794">CVE-2019-5794</a>

<p>Juno Im de Theori a découvert un problème d'usurpation d'interface
utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5795">CVE-2019-5795</a>

<p>pdknsk a découvert un problème de dépassement d'entier dans la
bibliothèque pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5796">CVE-2019-5796</a>

<p>Mark Brand a découvert une situation de compétition dans
l'implémentation des extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5797">CVE-2019-5797</a>

<p>Mark Brand a découvert une situation de compétition dans
l'implémentation de DOMStorage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5798">CVE-2019-5798</a>

<p>Tran Tien Hung a découvert un problème de lecture hors limites dans la
bibliothèque skia.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5799">CVE-2019-5799</a>

<p>sohalt a découvert une façon de contourner le « Content Security
Policy ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5800">CVE-2019-5800</a>

<p>Jun Kokatsu a découvert une façon de contourner le « Content Security
Policy ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5802">CVE-2019-5802</a>

<p>Ronni Skansing a découvert un problème d'usurpation d'interface
utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5803">CVE-2019-5803</a>

<p>Andrew Comminos a découvert une façon de contourner le « Content
Security Policy ».</p></li>

</ul>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 73.0.3683.75-1~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets chromium.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de chromium, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4421.data"
# $Id: $
