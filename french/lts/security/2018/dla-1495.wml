#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que le paquet git-annex possédait plusieurs vulnérabilités
lorsqu’il est utilisé avec des données non fiables. Cela pourrait conduire à
l’exécution de commande arbitraire et l’exfiltration de données chiffrées.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12976">CVE-2017-12976</a>

<p>git-annex avant 6.20170818 permettait à des attaquants distants d’exécuter
des commandes arbitraires à l'aide d'une URL SSH ayant un caractère tiret
initial dans le nom d’hôte, comme par exemple, avec ssh://-eProxyCommand= URL,
un problème concernant <a href="https://security-tracker.debian.org/tracker/CVE-2017-9800">CVE-2017-9800</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2017-12836">CVE-2017-12836</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2017-1000116">CVE-2017-1000116</a>, et
<a href="https://security-tracker.debian.org/tracker/CVE-2017-1000117">CVE-2017-1000117</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10857">CVE-2018-10857</a>

<p>git-annex est vulnérable à une exposition de données privées et à une attaque
par exfiltration. Il pourrait exposer le contenu de fichiers situés en dehors du
dépôt de git-annex, ou du contenu d’un serveur web privé sur localhost ou LAN.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10859">CVE-2018-10859</a>

<p>git-annex est vulnérable à une divulgation d’informations lors du déchiffrement
de fichiers. Un serveur contrefait pour une opération distante spéciale
pourrait duper git-annex pour déchiffrer un fichier qui était chiffré avec la
clef GPG de l’utilisateur. Cette attaque pourrait être utilisée pour divulguer
des données chiffrées qui n'avaient jamais été stockées dans git-annex</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 5.20141125+oops-1+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets git-annex.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1495.data"
# $Id: $
