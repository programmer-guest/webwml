<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>This regression update follows up on an upstream regression update [1]
regarding <a href="https://security-tracker.debian.org/tracker/CVE-2019-3859">CVE-2019-3859</a>.</p>

<p>With the previous libssh2 package revision, it was observed that user
authentication with private/public key pairs would fail under certain
circumstances.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.4.3-4.1+deb8u3.</p>

<p>We recommend that you upgrade your libssh2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>[1] <a href="https://github.com/libssh2/libssh2/pull/327">https://github.com/libssh2/libssh2/pull/327</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1730-2.data"
# $Id: $
