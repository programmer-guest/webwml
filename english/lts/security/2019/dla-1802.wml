<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been found in wireshark, a network traffic analyzer.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10894">CVE-2019-10894</a>

    <p>Assertion failure in dissect_gssapi_work (packet-gssapi.c) leading to
    crash of the GSS-API dissector. Remote attackers might leverage this
    vulnerability to trigger DoS via a packet containing crafted GSS-API
    payload.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10895">CVE-2019-10895</a>

    <p>Insufficient data validation leading to large number of heap buffer
    overflows read and write in the NetScaler trace handling module
    (netscaler.c). Remote attackers might leverage these vulnerabilities to
    trigger DoS, or any other unspecified impact via crafted packets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10899">CVE-2019-10899</a>

    <p>Heap-based buffer under-read vulnerability in the Service Location
    Protocol dissector. Remote attackers might leverage these
    vulnerabilities to trigger DoS, or any other unspecified impact via
    crafted SRVLOC packets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10901">CVE-2019-10901</a>

    <p>NULL pointer dereference in the Local Download Sharing Service
    protocol dissector. Remote attackers might leverage these flaws to
    trigger DoS via crafted LDSS packets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10903">CVE-2019-10903</a>

    <p>Missing boundary checks leading to heap out-of-bounds read
    vulnerability in the Microsoft Spool Subsystem protocol dissector.
    Remote attackers might leverage these vulnerabilities to trigger DoS,
    or any other unspecified impact via crafted SPOOLSS packets.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.12.1+g01b65bf-4+deb8u19.</p>

<p>We recommend that you upgrade your wireshark packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1802.data"
# $Id: $
