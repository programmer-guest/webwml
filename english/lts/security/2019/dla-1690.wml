<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in liblivemedia, the
LIVE555 RTSP server library:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6256">CVE-2019-6256</a>

    <p>liblivemedia servers with RTSP-over-HTTP tunneling enabled are
    vulnerable to an invalid function pointer dereference. This issue
    might happen during error handling when processing two GET and
    POST requests being sent with identical x-sessioncookie within
    the same TCP session and might be leveraged by remote attackers
    to cause DoS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7314">CVE-2019-7314</a>

    <p>liblivemedia servers with RTSP-over-HTTP tunneling enabled are
    affected by a use-after-free vulnerability. This vulnerability
    might be triggered by remote attackers to cause DoS (server crash)
    or possibly unspecified other impact.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2014.01.13-1+deb8u2.</p>

<p>We recommend that you upgrade your liblivemedia packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1690.data"
# $Id: $
