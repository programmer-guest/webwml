<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several security bugs have been identified and fixed in php5, a
server-side, HTML-embedded scripting language.  The affected components
include GD graphics, multi-byte string handling, phar file format
handling, and xmlrpc.</p>

<p>CVEs have not yet been assigned.  Once the CVE assignments are
announced, the Debian Security Tracker will be updated with the relevant
information.</p>

<p>For Debian 8 <q>Jessie</q>, this problems have been fixed in version
5.6.40+dfsg-0+deb8u1.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1679.data"
# $Id: $
