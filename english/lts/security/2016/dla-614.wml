<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in the Xen hypervisor. The
Common Vulnerabilities and Exposures project identifies the following
problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7092">CVE-2016-7092</a> (XSA-185)

    <p>Jeremie Boutoille of Quarkslab and Shangcong Luan of Alibaba
    discovered a flaw in the handling of L3 pagetable entries, allowing
    a malicious 32-bit PV guest administrator can escalate their
    privilege to that of the host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7094">CVE-2016-7094</a> (XSA-187)

    <p>x86 HVM guests running with shadow paging use a subset of the x86
    emulator to handle the guest writing to its own pagetables. Andrew
    Cooper of Citrix discovered that there are situations a guest can
    provoke which result in exceeding the space allocated for internal
    state. A malicious HVM guest administrator can cause Xen to fail a
    bug check, causing a denial of service to the host.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.1.6.lts1-2.  For Debian 8 <q>Jessie</q>, these problems have been fixed in
version 4.4.1-9+deb8u7.</p>

<p>We recommend that you upgrade your xen packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-614.data"
# $Id: $
