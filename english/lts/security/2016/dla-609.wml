<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update fixes the CVEs described below.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3857">CVE-2016-3857</a>

    <p>Chiachih Wu reported two bugs in the ARM OABI compatibility layer
    that can be used by local users for privilege escalation.  The
    OABI compatibility layer is enabled in all kernel flavours for
    armel and armhf.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4470">CVE-2016-4470</a>

    <p>Wade Mealing of the Red Hat Product Security Team reported that
    in some error cases the KEYS subsystem will dereference an
    uninitialised pointer.  A local user can use the keyctl()
    system call for denial of service (crash) or possibly for
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5696">CVE-2016-5696</a>

    <p>Yue Cao, Zhiyun Qian, Zhongjie Wang, Tuan Dao, and Srikanth V.
    Krishnamurthy of the University of California, Riverside; and Lisa
    M. Marvel of the United States Army Research Laboratory discovered
    that Linux's implementation of the TCP Challenge ACK feature
    results in a side channel that can be used to find TCP connections
    between specific IP addresses, and to inject messages into those
    connections.</p>

    <p>Where a service is made available through TCP, this may allow
    remote attackers to impersonate another connected user to the
    server or to impersonate the server to another connected user.  In
    case the service uses a protocol with message authentication
    (e.g. TLS or SSH), this vulnerability only allows denial of
    service (connection failure).  An attack takes tens of seconds, so
    short-lived TCP connections are also unlikely to be vulnerable.</p>

    <p>This may be mitigated by increasing the rate limit for TCP
    Challenge ACKs so that it is never exceeded:
        sysctl net.ipv4.tcp_challenge_ack_limit=1000000000</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5829">CVE-2016-5829</a>

    <p>Several heap-based buffer overflow vulnerabilities were found in
    the hiddev driver, allowing a local user with access to a HID
    device to cause a denial of service or potentially escalate their
    privileges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6136">CVE-2016-6136</a>

    <p>Pengfei Wang discovered that the audit subsystem has a
    'double-fetch' or <q>TOCTTOU</q> bug in its handling of special
    characters in the name of an executable.  Where audit logging of
    execve() is enabled, this allows a local user to generate
    misleading log messages.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6480">CVE-2016-6480</a>

    <p>Pengfei Wang discovered that the aacraid driver for Adaptec RAID
    controllers has a 'double-fetch' or <q>TOCTTOU</q> bug in its
    validation of <q>FIB</q> messages passed through the ioctl() system
    call.  This has no practical security impact in current Debian
    releases.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6828">CVE-2016-6828</a>

    <p>Marco Grassi reported a 'use-after-free' bug in the TCP
    implementation, which can be triggered by local users.  The
    security impact is unclear, but might include denial of service or
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7118">CVE-2016-7118</a>

    <p>Marcin Szewczyk reported that calling fcntl() on a file descriptor
    for a directory on an aufs filesystem would result in am <q>oops</q>.
    This allows local users to cause a denial of service.  This is a
    Debian-specific regression introduced in version 3.2.81-1.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.2.81-2.  This version also fixes a build failure (bug #827561) for
custom kernels with CONFIG_MODULES disabled, a regression in version
3.2.81-1.  It also updates the PREEMPT_RT featureset to version
3.2.81-rt117.</p>

<p>For Debian 8 <q>Jessie</q>, <a href="https://security-tracker.debian.org/tracker/CVE-2016-3857">CVE-2016-3857</a> has no impact; <a href="https://security-tracker.debian.org/tracker/CVE-2016-4470">CVE-2016-4470</a> and
<a href="https://security-tracker.debian.org/tracker/CVE-2016-5829">CVE-2016-5829</a> were fixed in linux version 3.16.7-ckt25-2+deb8u3 or
earlier; and the remaining issues are fixed in version 3.16.36-1+deb8u1.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-609.data"
# $Id: $
