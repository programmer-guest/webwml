<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7650">CVE-2017-7650</a>

<p>Pattern based ACLs can be bypassed by clients that set their username/client id to ‘#’ or ‘+’.
This allows locally or remotely connected clients to access MQTT topics that they do have the rights to.
The same issue may be present in third party authentication/access control plugins for Mosquitto.</p>

<p>The vulnerability only comes into effect where pattern based ACLs are in use,
or potentially where third party plugins are in use.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.15-2+deb7u1.</p>

<p>We recommend that you upgrade your mosquitto packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-961.data"
# $Id: $
