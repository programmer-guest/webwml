<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Security vulnerabilities have been identified in graphicsmagick, a
collection of image processing utilities and libraries.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13134">CVE-2017-13134</a>

    <p>Graphicsmagick was vulnerable to a heap-based buffer over-read and
    denial of service via a crafted SFW file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16547">CVE-2017-16547</a>

    <p>Graphicsmagick was vulnerable to a remote denial of service
    (application crash) or possible unspecified other impact via a
    crafted file resulting from a defective memory allocation.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.16-1.1+deb7u15.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1170.data"
# $Id: $
