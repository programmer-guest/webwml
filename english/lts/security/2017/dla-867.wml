<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple vulnerabilities has been found in audiofile.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6829">CVE-2017-6829</a>

    <p>Allows remote attackers to cause a denial of service (crash) via a
    crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6830">CVE-2017-6830</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-6834">CVE-2017-6834</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-6831">CVE-2017-6831</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-6832">CVE-2017-6832</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-6838">CVE-2017-6838</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-6839">CVE-2017-6839</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-6836">CVE-2017-6836</a>

    <p>Heap-based buffer overflow in that allows remote attackers to cause
    a denial of service (crash) via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6833">CVE-2017-6833</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-6835">CVE-2017-6835</a>

    <p>The runPull function allows remote attackers to cause a denial of
    service (divide-by-zero error and crash) via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6837">CVE-2017-6837</a>

    <p>Allows remote attackers to cause a denial of service (crash) via
    vectors related to a large number of coefficients.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.3.4-2+deb7u1.</p>

<p>We recommend that you upgrade your audiofile packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-867.data"
# $Id: $
