<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>WordPress, a web blogging tool, was affected by an issue where
$wpdb->prepare() can create unexpected and unsafe queries leading to
potential SQL injection (SQLi) in plugins and themes, as demonstrated
by a <q>double prepare</q> approach, a different vulnerability than
<a href="https://security-tracker.debian.org/tracker/CVE-2017-14723">CVE-2017-14723</a>.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.6.1+dfsg-1~deb7u18.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1160.data"
# $Id: $
