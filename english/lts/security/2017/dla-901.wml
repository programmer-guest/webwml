<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6448">CVE-2017-6448</a>

      <p>The dalvik_disassemble function in libr/asm/p/asm_dalvik.c in
      radare2 1.2.1 allows remote attackers to cause a denial of
      service (stack-based buffer overflow and application crash) or
      possibly have unspecified other impact via a crafted DEX file.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.9-3+deb7u2.</p>

<p>We recommend that you upgrade your radare2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-901.data"
# $Id: $
