<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The Cisco Talos team reported two sensitive security issues affecting
FreeXL-1.0.3 and any previous version.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2923">CVE-2017-2923</a>

    <p>An exploitable heap based buffer overflow vulnerability exists in
    the read_biff_next_record function of FreeXL 1.0.3. A specially
    crafted XLS file can cause a memory corruption resulting in remote
    code execution. An attacker can send malicious XLS file to trigger
    this vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2924">CVE-2017-2924</a>

    <p>An exploitable heap-based buffer overflow vulnerability exists in
    the read_legacy_biff function of FreeXL 1.0.3. A specially crafted
    XLS file can cause a memory corruption resulting in remote code
    execution. An attacker can send malicious XLS file to trigger this
    vulnerability.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.0.0b-1+deb7u4.</p>

<p>We recommend that you upgrade your freexl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1098.data"
# $Id: $
