<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Martin Thomson discovered that nss, the Mozilla Network Security Service
library, is prone to a use-after-free vulnerability in the TLS 1.2
implementation when handshake hashes are generated. A remote attacker
can take advantage of this flaw to cause an application using the nss
library to crash, resulting in a denial of service, or potentially to
execute arbitrary code.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:3.26-1+debu7u5.</p>

<p>We recommend that you upgrade your nss packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1138.data"
# $Id: $
