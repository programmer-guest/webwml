<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability has been discovered in Vim where a malformed spell file
could cause an integer overflow which is used as the size for memory
allocation, resulting in a subsequent buffer overflow.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:7.3.547-7+deb7u2.</p>

<p>We recommend that you upgrade your vim packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>Cheers,</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-822.data"
# $Id: $
