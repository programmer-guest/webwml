<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security vulnerability was discovered in OpenSSL, the Secure Sockets
Layer toolkit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3735">CVE-2017-3735</a>

    <p>It was discovered that OpenSSL is prone to a one-byte buffer
    overread while parsing a malformed IPAddressFamily extension in an
    X.509 certificate.</p>

    <p>Details can be found in the upstream advisory:
    <a href="https://www.openssl.org/news/secadv/20170828.txt">https://www.openssl.org/news/secadv/20170828.txt</a></p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.0.1t-1+deb7u3.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1157.data"
# $Id: $
