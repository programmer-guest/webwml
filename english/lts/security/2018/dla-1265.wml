<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Kerberos, a system for authenticating users and services on a network,
was affected by several vulnerabilities. The Common Vulnerabilities
and Exposures project identifies the following issues.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-1418">CVE-2013-1418</a>

    <p>Kerberos allows remote attackers to cause a denial of service
   (NULL pointer dereference and daemon crash) via a crafted request
    when multiple realms are configured.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-5351">CVE-2014-5351</a>

    <p>Kerberos sends old keys in a response to a -randkey -keepold
    request, which allows remote authenticated users to forge tickets by
    leveraging administrative access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-5353">CVE-2014-5353</a>

    <p>When the KDC uses LDAP, allows remote authenticated users to cause a
    denial of service (daemon crash) via a successful LDAP query with no
    results, as demonstrated by using an incorrect object type for a
    password policy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-5355">CVE-2014-5355</a>

    <p>Kerberos expects that a krb5_read_message data field is represented
    as a string ending with a '\0' character, which allows remote
    attackers to (1) cause a denial of service (NULL pointer
    dereference) via a zero-byte version string or (2) cause a denial of
    service (out-of-bounds read) by omitting the '\0' character,</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3119">CVE-2016-3119</a>

    <p>Kerberos allows remote authenticated users to cause a denial of
    service (NULL pointer dereference and daemon crash) via a crafted
    request to modify a principal.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3120">CVE-2016-3120</a>

    <p>Kerberos allows remote authenticated users to cause a denial of
    service (NULL pointer dereference and daemon crash) via an S4U2Self
    request.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.10.1+dfsg-5+deb7u9.</p>

<p>We recommend that you upgrade your krb5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1265.data"
# $Id: $
