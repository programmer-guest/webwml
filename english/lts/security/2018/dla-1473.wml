<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Francesco Sirocco discovered a privilege escalation flaw in otrs2, the
Open Ticket Request System. An attacker who is logged into OTRS as a
user may escalate their privileges by accessing a specially crafted URL.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.3.18-1+deb8u5.</p>

<p>We recommend that you upgrade your otrs2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1473.data"
# $Id: $
