<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Sean Bright discovered that Asterisk, a PBX and telephony toolkit,
contained a stack overflow vulnerability in the res_http_websocket.so
module that allowed remote attackers to crash Asterisk via specially
crafted HTTP requests to upgrade the connection to a websocket.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1:11.13.1~dfsg-2+deb8u6.</p>

<p>We recommend that you upgrade your asterisk packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1523.data"
# $Id: $
