<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Isaac Boukris and Andrew Bartlett discovered that the S4U2Self Kerberos
extension used in Samba's Active Directory support was susceptible to
man-in-the-middle attacks caused by incomplete checksum validation.</p>

<p>Details can be found in the upstream advisory at
<url "https://www.samba.org/samba/security/CVE-2018-16860.html" /></p>

<p>For the stable distribution (stretch), this problem has been fixed in
version 2:4.5.16+dfsg-1+deb9u2.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>For the detailed security status of samba please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4443.data"
# $Id: $
